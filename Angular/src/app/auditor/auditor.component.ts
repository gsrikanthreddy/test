﻿import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AlertService, AuthenticationService, UserService } from '../_services';
import { User } from '../_models';

@Component({ templateUrl: 'auditor.component.html' })
export class AuditorComponent implements OnInit {
    currentUser: User;
    users: User[] = [];
    constructor(private userService: UserService,private authenticationService: AuthenticationService) { 
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }

    ngOnInit() {
        this.loadAllUsers()
    }

    private loadAllUsers() {
        this.userService.getAll().pipe(first()).subscribe(users => {
            this.users = users;
        });
    }

    Logout(){
        this.authenticationService.Logout(this.currentUser.username).subscribe();
    }
}
